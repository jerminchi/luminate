package com.ch1bi.luminate;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class PersonAdapter extends ArrayAdapter<Person>
{
    private ArrayList<Person> personData;
    private Context context;
    private int layoutResourceId;

    public PersonAdapter(Context context, int layoutResourceId, ArrayList<Person> personData)
    {
        super(context, R.layout.list_row, personData);
        this.personData = personData;
    }

    @Override
    public int getCount()
    {
        return personData.size();
    }


    @Override
    public long getItemId(int position)
    {
        return position;
    }

    public static class ViewHolder
    {
        public TextView personName;
        public TextView personAge;
        public TextView personRace;
        public TextView personDeath;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;

        if(convertView == null)
        {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_row, parent, false);
            holder.personName =(TextView)convertView.findViewById(R.id.
                    name_title);
            holder.personAge = (TextView)convertView.findViewById(R.id.age_title);
            holder.personRace = (TextView)convertView.findViewById(R.id.race_title);
            holder.personDeath = (TextView)convertView.findViewById(R.id.death_title);

        }

        else
        {
            holder = (ViewHolder)convertView.getTag();

        }
        convertView.setTag(holder);

        holder.personName.setText("name: " + personData.get(position).getName());
        holder.personAge.setText("age: " + String.valueOf(personData.get(position).getAge()));
        holder.personRace.setText("race: " + personData.get(position).getRace());
        holder.personDeath.setText("death: " + String.valueOf(personData.get(position).getYear()));

        return convertView;
    }
}
