package com.ch1bi.luminate;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

public class PersonActivity extends AppCompatActivity
{
    private ListView dataView;
    PersonAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);

        if(getSupportActionBar()!= null)
        {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        dataView = (ListView)findViewById(R.id.data_view);
        ArrayList<Person> person = (ArrayList<Person>)getIntent().getSerializableExtra("PersonList");
        adapter = new PersonAdapter(PersonActivity.this, R.layout.list_row, person);
        dataView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
