package com.ch1bi.luminate;

import java.io.Serializable;

public class Person implements Serializable
{

        private String name;
        private int age;
        private String race;
        private int year;

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public int getAge()
        {
            return age;
        }

        public void setAge(int age)
        {
            this.age = age;
        }

        public String getRace()
        {
            return race;
        }

        public void setRace(String race)
        {
            this.race = race;
        }

        public int getYear()
        {
            return year;
        }

        public void setYear(int year)
        {
            this.year = year;
        }
    }
