package com.ch1bi.luminate;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity
{
    private static final String URL = "https://thecountedapi.com/api/counted";
    private ProgressBar spinningBar;
    private Button submitButton;
    private EditText editText;
    private TextView textView;

    ArrayList<Person> personData;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        assignViews();
        submitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                searchState();
            }
        });
    }

    private void assignViews()
    {
        spinningBar = (ProgressBar)findViewById(R.id.progress_bar);
        submitButton = (Button)findViewById(R.id.submit_button);
        editText = (EditText)findViewById(R.id.editText);
        textView = (TextView)findViewById(R.id.main_text);
    }

    private void startPersonActivity()
    {
        Intent intent = new Intent(this, PersonActivity.class);
        //Here we send our arrayList to the intent to PersonActivity
        intent.putExtra("PersonList", personData);
        startActivity(intent);
    }

    private void searchState()
    {
        textView.setVisibility(View.INVISIBLE);
        spinningBar.setVisibility(View.VISIBLE);
        //create object of okhttp
        OkHttpClient client = new OkHttpClient();

        //create request
        final Request request =  new Request.Builder()
                .url(URL+"/?state="+editText.getText().toString())
                .build();


        client.newCall(request).enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException
            {

                if(!response.isSuccessful())
                {
                    throw new IOException("Unexpected code " + response);
                }

                try
                {
                    //initialize arrayList
                    personData = new ArrayList<>();
                    //read data on worker thread
                    final String responseData = response.body().string();

                    //Create a Json Array so we are able to extract JSON objects
                    JSONArray jsonArray = new JSONArray(responseData);
                    for(int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String name = jsonObject.getString("name");
                        int age = jsonObject.getInt("age");
                        String race = jsonObject.getString("race");
                        int death = jsonObject.getInt("year");

                        //create new object of Person
                        //Based on the JSON objects we add them to our class
                        Person person = new Person();
                        person.setName(name);
                        person.setAge(age);
                        person.setRace(race);
                        person.setYear(death);

                        //add the data to list
                        personData.add(person);
                    }

                    //run code on main thread
                    MainActivity.this.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            spinningBar.setVisibility(View.GONE);
                            startPersonActivity();
                            textView.setVisibility(View.VISIBLE);
                        }
                    });
                }

                catch (JSONException j)
                {
                    j.printStackTrace();
                }
            }
        });

    }


}
